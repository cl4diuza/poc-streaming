# poc-streaming

## Getting started
1. install node js 

2. clone this repo

3. Install required 3rd party packages by executing the following command: 
    ```
    npm install
    ```

4. Run the application by executing the following command: 
    ```
    node Server.js
    ```

## Fork from repo

[Axinom-drm-quick-start](https://github.com/Axinom/drm-quick-start)


## Google storage


[Origin MP4](https://storage.googleapis.com/idolstream/file_example_MP4_480_1_5MG.mp4)

[File mpd](https://storage.googleapis.com/idolstream/sample.mpd)

[Video](https://storage.googleapis.com/idolstream/video.mp4)

[Audio](https://storage.googleapis.com/idolstream/audio.mp4)

## Encrpyt library

[Install Shaka Packager](https://google.github.io/shaka-packager/html/documentation.html#getting-shaka-packager)

[Encrypt using Widevin server](https://google.github.io/shaka-packager/html/tutorials/widevine.html)

