(function () {
    "use strict";

    // The responsibility of this module is to provide information about videos to other modules.

    // The videos are defined here. Add your videos to this list.
    let allVideos = [
        // Uncomment and copy-paste the block below as an example for adding custom
        // videos to the list.
        
        // {
        //     "name": "Example",
        //     "url": "https://storage.googleapis.com/idolstream/sample.mpd",
        //     "keys": [
        //         {
        //             "keyId": "d6a952c4-d67a-4d45-99d4-1fbab8a22023"
        //         }
        //     ]
        // },
        // node GenerateKeyUsingKeyService.js --signer widevine_test --signing-key 1ae8ccd0e7985cc0b6203a55855a1034afc252980e970ca90e5202689f947ab9 --signing-iv d58ce954203b7c9a9a9d467f59839249
        // Makemedia.exe --input C:\Users\ACER NITRO 5 I7 RTX\Downloads\video.mp4 --output "C:\Users\ACER NITRO 5 I7 RTX\idol-land\drm-quick-start\Website\Sample" --avc --keyid d6a952c4-d67a-4d45-99d4-1fbab8a22023 --key gOa4xu5g0CkRyqxNniHg3w==

        // The following entries are for the pre-generated demo videos. To add your own videos, 
        // copy the sample video above, append to the list and adjust the fields as needed.
        //
        // Note: The demo videos have hardcoded license tokens for maximum ease of use of the
        // sample app. Never do this in production - always generate a new license token on
        // every request.

        // Note: the "tags" property is optional. The demo player uses this to filter the video
        // list -- for example, to only display FairPlay-compliant videos on Safari.
        {
            "name": "Axinom demo video - single key (DASH; cenc)",
            "url": "https://storage.googleapis.com/idolstream/sample.mpd",
            "licenseToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2ZXJzaW9uIjogMSwKImJlZ2luX2RhdGUiOiAiMjAwMC0wMS0wMVQxNzozNzowNCswMzowMCIsCiJleHBpcmF0aW9uX2RhdGUiOiAiMjAyNS0xMi0zMVQyMzo1OTo0MCswMzowMCIsCiJjb21fa2V5X2lkIjogImQ2YTk1MmM0LWQ2N2EtNGQ0NS05OWQ0LTFmYmFiOGEyMjAyMyIsCiJtZXNzYWdlIjogewogICJ2ZXJzaW9uIjogMSwKICAiY29tX2tleV9pZCI6ICJiMzM2NGViNS01MWY2LTRhZTMtOGM5OC0zM2NlZDVlMzFjNzgiLAogICJtZXNzYWdlIjogewogICAgInR5cGUiOiAiZW50aXRsZW1lbnRfbWVzc2FnZSIsCiAgICAiZmlyc3RfcGxheV9leHBpcmF0aW9uIjogNjAsCiAgICAicGxheXJlYWR5IjogewogICAgICAicmVhbF90aW1lX2V4cGlyYXRpb24iOiB0cnVlCiAgICB9LAogICAgImtleXMiOiBbCiAgICAgIHsKICAgICAgICAiaWQiOiAiYWJiYTI3MWUtOGJjZi01NTJiLWJkMmUtODZhNDM0YTlhNWQ5IiwKICAgICAgICAiZW5jcnlwdGVkX2tleSI6ICJkNThjZTk1NDIwM2I3YzlhOWE5ZDQ2N2Y1OTgzOTI0OSIKICAgICAgfQogICAgXQogIH0KfX0.AZWkqITqRGeGggAJyeopfXovsAHXIzjoCcm4YkQ_1gw",
        },
        {
            "name": "Axinom demo video - single key (HLS; cbcs)",
            "url": "https://media.axprod.net/VTB/DrmQuickStart/AxinomDemoVideo-SingleKey/Encrypted_Cbcs/Manifest.m3u8",
            "licenseToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2ZXJzaW9uIjoxLCJjb21fa2V5X2lkIjoiNjllNTQwODgtZTllMC00NTMwLThjMWEtMWViNmRjZDBkMTRlIiwibWVzc2FnZSI6eyJ2ZXJzaW9uIjoyLCJ0eXBlIjoiZW50aXRsZW1lbnRfbWVzc2FnZSIsImxpY2Vuc2UiOnsiYWxsb3dfcGVyc2lzdGVuY2UiOnRydWV9LCJjb250ZW50X2tleXNfc291cmNlIjp7ImlubGluZSI6W3siaWQiOiIyMTFhYzFkYy1jOGEyLTQ1NzUtYmFmNy1mYTRiYTU2YzM4YWMiLCJ1c2FnZV9wb2xpY3kiOiJUaGVPbmVQb2xpY3kifV19LCJjb250ZW50X2tleV91c2FnZV9wb2xpY2llcyI6W3sibmFtZSI6IlRoZU9uZVBvbGljeSIsInBsYXlyZWFkeSI6eyJwbGF5X2VuYWJsZXJzIjpbIjc4NjYyN0Q4LUMyQTYtNDRCRS04Rjg4LTA4QUUyNTVCMDFBNyJdfX1dfX0.D9FM9sbTFxBmcCOC8yMHrEtTwm0zy6ejZUCrlJbHz_U",
            "tags": ["FairPlay"]
        },
        {
            "name": "Axinom demo video - multikey (DASH; cenc)",
            "url": "https://storage.googleapis.com/idolstream/sample.mpd",
            "licenseToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2ZXJzaW9uIjogMSwKImJlZ2luX2RhdGUiOiAiMjAwMC0wMS0wMVQxNzozNzowNCswMzowMCIsCiJleHBpcmF0aW9uX2RhdGUiOiAiMjAyNS0xMi0zMVQyMzo1OTo0MCswMzowMCIsCiJjb21fa2V5X2lkIjogImQ2YTk1MmM0LWQ2N2EtNGQ0NS05OWQ0LTFmYmFiOGEyMjAyMyIsCiJtZXNzYWdlIjogewogICJ2ZXJzaW9uIjogMSwKICAiY29tX2tleV9pZCI6ICJiMzM2NGViNS01MWY2LTRhZTMtOGM5OC0zM2NlZDVlMzFjNzgiLAogICJtZXNzYWdlIjogewogICAgInR5cGUiOiAiZW50aXRsZW1lbnRfbWVzc2FnZSIsCiAgICAiZmlyc3RfcGxheV9leHBpcmF0aW9uIjogNjAsCiAgICAicGxheXJlYWR5IjogewogICAgICAicmVhbF90aW1lX2V4cGlyYXRpb24iOiB0cnVlCiAgICB9LAogICAgImtleXMiOiBbCiAgICAgIHsKICAgICAgICAiaWQiOiAiYWJiYTI3MWUtOGJjZi01NTJiLWJkMmUtODZhNDM0YTlhNWQ5IiwKICAgICAgICAiZW5jcnlwdGVkX2tleSI6ICJkNThjZTk1NDIwM2I3YzlhOWE5ZDQ2N2Y1OTgzOTI0OSIKICAgICAgfQogICAgXQogIH0KfX0.AZWkqITqRGeGggAJyeopfXovsAHXIzjoCcm4YkQ_1gw",
        },
        {
            "name": "Axinom demo video - multikey (HLS; cbcs)",
            "url": "https://media.axprod.net/VTB/DrmQuickStart/AxinomDemoVideo-MultiKey/Encrypted_Cbcs/Manifest.m3u8",
            "licenseToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2ZXJzaW9uIjoxLCJjb21fa2V5X2lkIjoiNjllNTQwODgtZTllMC00NTMwLThjMWEtMWViNmRjZDBkMTRlIiwibWVzc2FnZSI6eyJ2ZXJzaW9uIjoyLCJ0eXBlIjoiZW50aXRsZW1lbnRfbWVzc2FnZSIsImxpY2Vuc2UiOnsiYWxsb3dfcGVyc2lzdGVuY2UiOnRydWV9LCJjb250ZW50X2tleXNfc291cmNlIjp7ImlubGluZSI6W3siaWQiOiJmM2Q1ODhjNy1jMTdhLTQwMzMtOTAzNS04ZGIzMTczOTBiZTYiLCJ1c2FnZV9wb2xpY3kiOiJUaGVPbmVQb2xpY3kifSx7ImlkIjoiNDRiMThhMzItNmQzNi00OTlkLThiOTMtYTIwZjk0OGFjNWYyIiwidXNhZ2VfcG9saWN5IjoiVGhlT25lUG9saWN5In0seyJpZCI6ImFlNmU4N2UyLTNjM2MtNDZkMS04ZTlkLWVmNGM0NjFkNDY4MSIsInVzYWdlX3BvbGljeSI6IlRoZU9uZVBvbGljeSJ9XX0sImNvbnRlbnRfa2V5X3VzYWdlX3BvbGljaWVzIjpbeyJuYW1lIjoiVGhlT25lUG9saWN5IiwicGxheXJlYWR5Ijp7InBsYXlfZW5hYmxlcnMiOlsiNzg2NjI3RDgtQzJBNi00NEJFLThGODgtMDhBRTI1NUIwMUE3Il19fV19fQ.DpwBd1ax4Z7P0cCOZ7ZJMotqVWfLFCj2DYdH37xjGxM",
            "tags": ["FairPlay"]
        }
    ];

    // Verifies that all critical information is present on a video.
    // Automatically performs sanity checks to avoid making mistakes in the above list. 
    function verifyVideoIntegrity(video) {
        if (!video)
            throw new Error("A video was expected but was not present.");
        if (!video.name || !video.name.length)
            throw new Error("A video is missing its name.");

        console.log("Verifying integrity of video definition: " + video.name);

        if (!video.url || !video.url.length)
            throw new Error("The video is missing its URL.");

        // Either a hardcoded license token or the keys structure must exist. Not both.
        if (video.licenseToken && video.keys)
            throw new Error("The video has both a hardcoded license token and a content key list - pick only one.");
        if (!video.licenseToken && !video.keys)
            throw new Error("The video is missing the content key list.");

        if (video.keys) {
            if (!video.keys.length)
                throw new Error("The content key list for this video is empty.");

            // Verify that each item in the keys list has all the required data.
            video.keys.forEach(function verifyKey(item) {
                if (!item.keyId)
                    throw new Error("A content key is missing the key ID.");
            });
        }
    }

    // Verify all videos on startup.
    allVideos.forEach(verifyVideoIntegrity);

    module.exports = {
        "getAllVideos": function getAllVideos() {
            return allVideos;
        },
        "getVideoByName": function getVideoByName(name) {
            return allVideos.find(function filter(item) {
                return item.name === name;
            });
        }
    };
})();